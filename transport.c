/*
 * transport.c 
 *
 * CS244a HW#3 (Reliable Transport)
 *
 * This file implements the STCP layer that sits between the
 * mysocket and network layers. You are required to fill in the STCP
 * functionality in this file. 
 *
 */

#include <time.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <arpa/inet.h>
#include "mysock.h"
#include "stcp_api.h"
#include "transport.h"

// define constants
#define DATA_OFFSET 5
#define RECV_WINDOW_SIZE 3072
#define MAX_PAYLOAD_SIZE 536
#define MSS 536 // max sement size (for window)
#define MAX_PCK_SIZE 556 // max payload size + header size
#define THRESHOLD 2144
#define MAX_LIFETIME 120

// file transfer direction
#define SEND 0
#define RECV 1

// STCP states 
enum { CSTATE_ESTABLISHED, CSTATE_LISTEN, CSTATE_SYN_SENT, CSTATE_SYN_RCVD,
       CSTATE_FIN_1, CSTATE_FIN_2, CSTATE_TIME_WAIT,
       CSTATE_CLOSE_WAIT, CSTATE_LAST_ACK,
       CSTATE_CLOSING,
       CSTATE_CLOSED };

typedef uint32_t win_size;
/* TCP context */
/* this structure is global to a mysocket descriptor */
typedef struct
{
    bool_t done;    /* TRUE once connection is closed */

    int connection_state;   /* state of the connection (established, etc.) */
    tcp_seq initial_sequence_num;

    /* any other connection-wide global variables go here */
    tcp_seq seq_num; // current sequence number
    tcp_seq ack_num; // current acked number (expected to recv next)

    // sender window
    win_size swnd_start;
    win_size swnd_end;
    // receive window
    win_size rwnd_start;
    win_size rwnd_end;

    // whether client or server
    bool_t is_active;

    // log file
    FILE *client_fp;
    FILE *server_fp;

} context_t;


static void generate_initial_seq_num(context_t *ctx);
static void control_loop(mysocket_t sd, context_t *ctx);

/* ---------------- my own function ----------------- */
// create
STCPHeader* create_non_data_pck(tcp_seq seq_num, tcp_seq ack_num, uint8_t flag);
STCPHeader* create_data_pck(tcp_seq seq_num, tcp_seq ack_num, size_t data_byte);

// send & wait
void send_pck(mysocket_t sd, STCPHeader* sth, size_t size, uint8_t flag);
bool_t wait_pck(mysocket_t sd, context_t *ctx, uint8_t flag);
bool_t wait_ACK_to_expand(mysocket_t sd, context_t *ctx, uint8_t flag, bool_t is_active);

// print
void print_log(context_t *ctx, win_size swnd, win_size rem, size_t byte, bool_t is_active, bool_t mode);
/* --------------------------------------------------- */


/* initialise the transport layer, and start the main loop, handling
 * any data from the peer or the application.  this function should not
 * return until the connection is closed.
 */
void transport_init(mysocket_t sd, bool_t is_active)
{
    context_t *ctx;
    STCPHeader *sth;

    ctx = (context_t *) calloc(1, sizeof(context_t));
    assert(ctx);

    generate_initial_seq_num(ctx);

    /* XXX: you should send a SYN packet here if is_active, or wait for one
     * to arrive if !is_active.  after the handshake completes, unblock the
     * application with stcp_unblock_application(sd).  you may also use
     * this to communicate an error condition back to the application, e.g.
     * if connection fails; to do so, just set errno appropriately (e.g. to
     * ECONNREFUSED, etc.) before calling the function.
     */
    if (is_active){
        // 1) create & send SYN pck
        sth = create_non_data_pck(ctx->initial_sequence_num, 1, TH_SYN);
        send_pck(sd, sth, sizeof(STCPHeader), TH_SYN);
        ctx->connection_state = CSTATE_SYN_SENT;

        // 2) wait SYN+ACK
        wait_pck(sd, ctx, (TH_SYN | TH_ACK));

        // 3) create & send ACK
        sth = create_non_data_pck(ctx->initial_sequence_num, ctx->ack_num, TH_ACK);
        send_pck(sd, sth, sizeof(STCPHeader), TH_ACK);
    }
    else{
        ctx->connection_state = CSTATE_LISTEN;
        // 1) wait SYN
        wait_pck(sd, ctx, TH_SYN);
        ctx->connection_state = CSTATE_SYN_RCVD;

        // 2) create & send SYN+ACK
        sth = create_non_data_pck(ctx->initial_sequence_num, ctx->ack_num, (TH_SYN | TH_ACK));
        send_pck(sd, sth, sizeof(STCPHeader), (TH_SYN | TH_ACK));

        // 3) wait ACK
        wait_pck(sd, ctx, TH_ACK);
    }

    // i) connection is established
    ctx->connection_state = CSTATE_ESTABLISHED;
    ctx->is_active = is_active;
    // ii) connection refused (error)

    stcp_unblock_application(sd);

    // data transfer
    control_loop(sd, ctx);

    /* do any cleanup here */
    fclose(ctx->client_fp);
    fclose(ctx->server_fp);
    free(ctx);
}


/* generate initial sequence number for an STCP connection */
static void generate_initial_seq_num(context_t *ctx)
{
    assert(ctx);
    ctx->done = 0;
    ctx->initial_sequence_num = 1;
    ctx->seq_num = 1;
    ctx->ack_num = 1;

    // window inittialization
    ctx->swnd_start = 1;
    ctx->swnd_end = MAX_PAYLOAD_SIZE; // 536
    ctx->rwnd_start = 1;
    ctx->rwnd_end = RECV_WINDOW_SIZE; // 3072

    // log file
    ctx->client_fp = fopen("client_log.txt", "w");
    ctx->server_fp = fopen("server_log.txt", "w");
}


/* control_loop() is the main STCP loop; it repeatedly waits for one of the
 * following to happen:
 *   - network: incoming data from the peer
 *   - app:     new data from the application (via mywrite())
 *   - close:   the socket to be closed (via myclose())
 *   - a timeout
 */
static void control_loop(mysocket_t sd, context_t *ctx)
{   
    assert(ctx); 

    // initialize for data transfer
    ctx->seq_num = 1;
    ctx->ack_num = 1;

    // while connection is not finished
    while (!ctx->done)
    {     
        win_size swnd, rem;

         // get an event
        unsigned int event;
        event = stcp_wait_for_event(sd, ANY_EVENT, NULL);

        /* check whether it was the network, app, or a close request */

        // i) new data from the application (via mywrite() )
        if (event & APP_DATA){ 
            /* the application has requested that data be sent */

            rem = ctx->swnd_end - ctx->seq_num + 1;
            char *app_data = (char *) calloc(1, MIN(rem, (win_size) MAX_PAYLOAD_SIZE));
            
            assert(app_data);
            size_t app_byte = stcp_app_recv(sd, app_data, MIN(rem, (win_size) MAX_PAYLOAD_SIZE));
            rem = ctx->swnd_end - ctx->seq_num + 1;
            // swnd is fulled. wait ACK..
            if (rem == 0){
                wait_ACK_to_expand(sd, ctx, TH_ACK, ctx->is_active);
                continue;
            }
            
            // if data is in swnd range
            assert(ctx->seq_num+app_byte-1 <= ctx->swnd_end);
            if (ctx->seq_num+app_byte-1 <= ctx->swnd_end){ 
                // i-1) create & send pck
                STCPHeader *sth = (STCPHeader *) calloc(1, sizeof(STCPHeader)+app_byte);
                assert(sth);
                sth->th_seq = htonl(ctx->seq_num);
                sth->th_ack = htonl(ctx->ack_num);
                sth->th_off = 5;
                sth->th_flags = 0;
                memcpy((char *)sth+sizeof(STCPHeader), app_data, app_byte);

                ssize_t net_send_byte = stcp_network_send(sd, sth, sizeof(STCPHeader)+app_byte, NULL);
                assert(net_send_byte > 0);

                // i-2) print log
                win_size swnd = ctx->swnd_end - ctx->swnd_start + 1;
                win_size rem = ctx->swnd_end - ctx->seq_num + 1;
                print_log(ctx, swnd, rem, app_byte, ctx->is_active, SEND);

                // i-3) post-process for sliding window
                ctx->seq_num += app_byte;

                free(app_data);
                free(sth);
            }
            else {
                assert(0);
            }
            
        }

        // ii) network: incoming data from the peer
        else if (event & NETWORK_DATA){
            /* there is something to receive */
            // receive pck
            char *net_data = (char *) calloc(1, MAX_PCK_SIZE);
            assert(net_data);
            size_t net_byte = stcp_network_recv(sd, net_data, MAX_PCK_SIZE);
            assert(net_byte > 0);
            STCPHeader *sth = (STCPHeader *) net_data;
            assert(sth);

            // packet is ACK
            if (sth->th_flags & TH_ACK){
                assert(net_byte - sizeof(STCPHeader) > 0); // original data should be exist

                // ii-1) print log
                swnd = ctx->swnd_end - ctx->swnd_start + 1;
                rem = ctx->swnd_end - ctx->seq_num + 1;
                print_log(ctx, swnd, rem, net_byte - sizeof(STCPHeader), ctx->is_active, RECV);

                // ii-2) post-process for sliding window
                // expand swnd: slow-start
                if (swnd < THRESHOLD)
                    swnd += MAX_PAYLOAD_SIZE;
                else
                    swnd = MIN(swnd + MAX_PAYLOAD_SIZE*MAX_PAYLOAD_SIZE/swnd, RECV_WINDOW_SIZE);
                ctx->swnd_end = ctx->swnd_start + swnd - 1;

                // sliding window
                ctx->swnd_start += net_byte - sizeof(STCPHeader);
                ctx->swnd_end += net_byte - sizeof(STCPHeader);
                
            }

            if (sth->th_flags & TH_FIN){
                // 1) create & send ACK
                ctx->ack_num = ntohl(sth->th_seq)+ 1;
                STCPHeader* sth_finack = create_non_data_pck(ctx->seq_num, ctx->ack_num, TH_ACK);
                assert(sth_finack);

                send_pck(sd, sth_finack, sizeof(STCPHeader), TH_ACK);
                ctx->connection_state = CSTATE_CLOSE_WAIT;

                // 2) create & send FIN
                STCPHeader* sth_fin2 = create_non_data_pck(99, 99, TH_FIN);
                assert(sth_fin2);
                send_pck(sd, sth_fin2, sizeof(STCPHeader), TH_FIN);
                ctx->connection_state = CSTATE_LAST_ACK;

                // 3) wait ACK
                wait_pck(sd, ctx, TH_ACK);

                // 4) close
                ctx->connection_state = CSTATE_CLOSED;
                ctx->done = 1;
                break;
            }

            // if data from peer APP
            if (sth->th_flags == 0){
                size_t data_byte = net_byte - sizeof(STCPHeader);
                // 1) send to my APP
                stcp_app_send(sd, (char *) net_data+sizeof(STCPHeader), data_byte);

                // 2) send ACK to peer
                STCPHeader *ack_sth = create_data_pck(ctx->seq_num, ctx->ack_num+data_byte, data_byte);
                send_pck(sd, ack_sth, sizeof(STCPHeader)+data_byte, TH_ACK);

                // 3) post process
                ctx->ack_num += data_byte;
                ctx->rwnd_start += data_byte;
                ctx->rwnd_end += data_byte;
            }   


            free(net_data);
        }
        
        // iii) close: the socket to be closed (via myclose())
        else if (event & APP_CLOSE_REQUESTED){
            /* 4-way handshake */
            // iii-1) create & send FIN
            STCPHeader* sth_fin = create_non_data_pck(ctx->seq_num, 0, TH_FIN);
            assert(sth_fin);
            send_pck(sd, sth_fin, sizeof(STCPHeader), TH_FIN);
            ctx->connection_state = CSTATE_FIN_1;
            
            // iii-2) wait ACK
            wait_pck(sd, ctx, TH_ACK);

            // iii-3) wait FIN
            ctx->connection_state = CSTATE_FIN_2;
            wait_pck(sd, ctx, TH_FIN);

            // iii-4) create & send ACK
            ctx->connection_state = CSTATE_TIME_WAIT;
            STCPHeader* sth_ack = create_non_data_pck(ctx->seq_num, ctx->ack_num, TH_ACK);
            assert(sth_ack);
            send_pck(sd, sth_ack, sizeof(STCPHeader), TH_ACK);
            
            // close connection
            ctx->connection_state = CSTATE_CLOSED;
            ctx->done = 1;
            
        }

        else {
            ;
        }

    }
}

/**********************************************************************/
/* make STCP Header for packets related to connection initiation/termination
 * - args
 * seq_num: sequence number
 * ack_num: acked number
 * flag: whether SYN, SYN_ACK, ACK, FIN
 * 
 * - return: STCP header pointer
 */
STCPHeader* create_non_data_pck(tcp_seq seq_num, tcp_seq ack_num, uint8_t flag)
{   
    STCPHeader *sth;
    sth = (STCPHeader *) calloc(1, sizeof(STCPHeader));
    assert(sth);

    sth->th_seq = htonl(seq_num);
    sth->th_ack = htonl(ack_num);
    sth->th_off = DATA_OFFSET;
    sth->th_flags = flag;
    sth->th_win = htons(RECV_WINDOW_SIZE);

    return sth;
}


STCPHeader* create_data_pck(tcp_seq seq_num, tcp_seq ack_num, size_t data_byte)
{   
    STCPHeader *sth;
    sth = (STCPHeader *) calloc(1, sizeof(STCPHeader) + data_byte);
    assert(sth);

    sth->th_seq = htonl(seq_num);
    sth->th_ack = htonl(ack_num);
    sth->th_off = DATA_OFFSET;
    sth->th_flags = TH_ACK;
    sth->th_win = htons(RECV_WINDOW_SIZE);

    return sth;
}


/* send STCP Header via sd
 * - args
 * sd: my socket
 * sth: STCP Header
 * flag: whether SYN, SYN_ACK, ACK, FIN
 * 
 * - return: none
 */
void send_pck(mysocket_t sd, STCPHeader* sth, size_t size, uint8_t flag)
{   
    assert(sth);
    stcp_network_send(sd, sth, size, NULL);

    // free un-neccessary one
    free(sth);
    return;
}


/* receive STCP Header via sd
 * - args
 * sd: my socket
 * ctx: current context
 * flag: whether SYN, SYN_ACK, ACK, FIN
 * 
 * - return: 1(true) or 0(false)
 */
bool_t wait_pck(mysocket_t sd, context_t *ctx, uint8_t flag)
{   
    unsigned int event = stcp_wait_for_event(sd, ANY_EVENT, NULL);
    char *recv = (char *) calloc(1, sizeof(STCPHeader));
    assert(recv);

    if (event & NETWORK_DATA){
        // receive pck from peer
        ssize_t recv_bytes = stcp_network_recv(sd, recv, sizeof(STCPHeader));
        assert(recv_bytes > 0);
        STCPHeader *recv_sth = (STCPHeader *) recv;

        if (recv_sth->th_flags & flag){
            ctx->ack_num = ntohl(recv_sth->th_seq) + 1;
            free(recv);
            return 1;
        }
        else{
            ;
        }

    }
    else {
        ;
    }

    free(recv);
    return 0;
}


/* wait ACK from peer to expand cwnd
 * - args
 * sd: my socket
 * ctx: current context
 * flag: whether SYN, SYN_ACK, ACK, FIN
 * is_active: whether this context is client or server
 * 
 * - return: 1(true) or 0(false)
 */
bool_t wait_ACK_to_expand(mysocket_t sd, context_t *ctx, uint8_t flag, bool_t is_active)
{   
    win_size swnd, rem;
    unsigned int event = stcp_wait_for_event(sd, NETWORK_DATA, NULL);
    char *recv = (char *) calloc(1, MAX_PCK_SIZE);
    assert(recv);

    if (event & NETWORK_DATA){
        // receive pck from peer
        ssize_t net_byte = stcp_network_recv(sd, recv, MAX_PCK_SIZE);
        assert(net_byte > 0);
        STCPHeader *recv_sth = (STCPHeader *) recv;

        if (recv_sth->th_flags & flag){
            assert(net_byte - sizeof(STCPHeader) > 0); // original data should be exist
            // ii-1) print log
            swnd = ctx->swnd_end - ctx->swnd_start + 1;
            rem = ctx->swnd_end - ctx->seq_num + 1;
            print_log(ctx, swnd, rem, net_byte - sizeof(STCPHeader), is_active, RECV);

            // ii-2) post-process for sliding window
            // expand swnd: slow-start
            if (swnd < THRESHOLD)
                swnd += MAX_PAYLOAD_SIZE;
            else
                swnd = MIN(swnd + MAX_PAYLOAD_SIZE*MAX_PAYLOAD_SIZE/swnd, RECV_WINDOW_SIZE);
            ctx->swnd_end = ctx->swnd_start + swnd - 1;

            // sliding window
            ctx->swnd_start += net_byte - sizeof(STCPHeader);
            ctx->swnd_end += net_byte - sizeof(STCPHeader);

            free(recv);
            return 1;
        }
        else{
            ;
        }


    }
    
    free(recv);
    return 0;
    
}



/* printf log (swnd, Rem, Byte) whenever the following cases:
 *   - swnd: full sender window size
 *   - Rem: window size that usable but not yet sent in sender window
 *          => swnd - (NextSwqNum - SendBase)
 *   - Byte: size of data which is sent or is ACKed
 */
void print_log(context_t *ctx, win_size swnd, win_size rem, size_t byte, bool_t is_active, bool_t mode)
{
    // i) client
    if (is_active){
        // write to log file
        if (mode == SEND)
            fprintf(ctx->client_fp, "Send:\t%u\t%u\t%zu\n", swnd, rem, byte);
        else if (mode == RECV)
            fprintf(ctx->client_fp, "Recv:\t%u\t%u\t%zu\n", swnd, rem, byte);
    }

    // ii) server
    else{
        // write to log file
        if (mode == SEND)
            fprintf(ctx->server_fp, "Send:\t%u\t%u\t%zu\n", swnd, rem, byte);
        else if (mode == RECV)
            fprintf(ctx->server_fp, "Recv:\t%u\t%u\t%zu\n", swnd, rem, byte);
    }

    

}
/**********************************************************************/





/* our_dprintf
 *
 * Send a formatted message to stdout.
 * 
 * format               A printf-style format string.
 *
 * This function is equivalent to a printf, but may be
 * changed to log errors to a file if desired.
 *
 * Calls to this function are generated by the dprintf amd
 * dperror macros in transport.h
 */
void our_dprintf(const char *format,...)
{
    va_list argptr;
    char buffer[1024];

    assert(format);
    va_start(argptr, format);
    vsnprintf(buffer, sizeof(buffer), format, argptr);
    va_end(argptr);
    fputs(buffer, stdout);
    fflush(stdout);
}